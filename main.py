from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
@app.route('/home')
def mainpage():
    return render_template('hello.html')

@app.route('/CDF')
def CDFpage():
    return render_template('CDF.html')

if __name__ == '__main__':
    app.run(debug=True)


